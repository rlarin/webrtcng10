const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('./config');
const pathPrefix = '/api';

// Post Model

// Generate a JWT token to authenticate and make Zoom API calls
const payload = {
  iss: config.ZOOM_API_KEY,
  exp: ((new Date()).getTime() + 5000)
};
const token = jwt.sign(payload, config.ZOOM_API_SECRET);

const app = express();

mongoose.connect("mongodb+srv://rlarin:DDWEW0c1p0NLasDZ@cluster0.10jhx.mongodb.net/meanStackDB?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('Connected to database!');
  })
  .catch((e) => {
    console.log('Connection Failed!', e);
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Middleware (app.use) to include headers
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, DELETE, OPTIONS'
  );
  next();
});

// GETs
app.get('/', (request, response) => {
  response.send("Power your app with Webhooks!");

});

app.get(pathPrefix + '/ping', ((req, res, next) => {
  res.status(200).json({
    message: 'pong!'
  });
}));

// POSTs


module.exports = app;
