import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MaterialModule} from './modules/material/material.module';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ZoomComponent } from './views/web-rtc/zoom/zoom.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import {HttpClientModule} from '@angular/common/http';
import { MicrosoftComponent } from './views/web-rtc/microsoft/microsoft.component';
import { JoinZoomMeetingComponent } from './views/web-rtc/zoom/join-zoom-meeting/join-zoom-meeting.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ZoomLiveStreamingComponent } from './views/web-rtc/zoom/zoom-live-streaming/zoom-live-streaming.component';
import { ZoomMeetingRecorderComponent } from './views/web-rtc/zoom/zoom-meeting-recorder/zoom-meeting-recorder.component';
import { ZoomDemoComponent } from './views/web-rtc/zoom/zoom-demo/zoom-demo.component';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    ZoomComponent,
    MainLayoutComponent,
    MicrosoftComponent,
    JoinZoomMeetingComponent,
    ZoomLiveStreamingComponent,
    ZoomMeetingRecorderComponent,
    ZoomDemoComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
