import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {MainLayoutComponent} from './layouts/main-layout/main-layout.component';
import {MicrosoftComponent} from './views/web-rtc/microsoft/microsoft.component';
import {JoinZoomMeetingComponent} from './views/web-rtc/zoom/join-zoom-meeting/join-zoom-meeting.component';
import {ZoomLiveStreamingComponent} from './views/web-rtc/zoom/zoom-live-streaming/zoom-live-streaming.component';
import {ZoomMeetingRecorderComponent} from './views/web-rtc/zoom/zoom-meeting-recorder/zoom-meeting-recorder.component';
import {ZoomDemoComponent} from './views/web-rtc/zoom/zoom-demo/zoom-demo.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      // {path: 'web-rtc/zoom', component: ZoomComponent},
      {path: 'web-rtc/zoom/zoom-demo', component: ZoomDemoComponent},
      {path: 'web-rtc/zoom/join-zoom-meeting', component: JoinZoomMeetingComponent},
      {path: 'web-rtc/zoom/zoom-meeting-recorder', component: ZoomMeetingRecorderComponent},
      {path: 'web-rtc/zoom/zoom-live-streaming', component: ZoomLiveStreamingComponent},
      {path: 'web-rtc/ms', component: MicrosoftComponent},
    ]
  },
  {path: '**', component: AppComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
