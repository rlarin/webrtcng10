import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-microsoft',
  templateUrl: './microsoft.component.html',
  styleUrls: ['./microsoft.component.less']
})
export class MicrosoftComponent implements OnInit {

  public appSettings = {
    clientId: '11b96bf3-cf08-4f1e-ad4f-a2fb78d33f8e',
  };
  constructor() { }

  ngOnInit(): void {
  }

}
