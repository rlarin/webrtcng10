import {Component, Inject, OnInit} from '@angular/core';

import {DOCUMENT} from '@angular/common';
import {HttpClient} from '@angular/common/http';

import {ZoomMtg} from '@zoomus/websdk';

declare var $: any;

ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

@Component({
  selector: 'app-zoom',
  templateUrl: './zoom.component.html',
  styleUrls: ['./zoom.component.less']
})
export class ZoomComponent implements OnInit {
  public constraints = {
    audio: true,
    video: true
  };
  public streamCard1: any;

  public signatureEndpoint = 'https://zoomjwtapp.herokuapp.com/'; // https://08f6a9c298da.ngrok.io/';
  public apiKey = 'Aa1I2GFDRceWeQcuCpRWZw';
  public meetingNumber = '3044848838'; // '93466917886'; https://zoom.us/j/92010732259?pwd=dEc3M3AxKzBtdmFmNU5FVkJDSnZSUT09
  public role = 0;
  public leaveUrl = '/web-rtc/zoom';
  public userName = 'tessa';
  public userEmail = '';
  public passWord = 'du40j4';

  constructor(
    public http: HttpClient,
    @Inject(DOCUMENT) document
  ) { }

  ngOnInit(): void {
   // const zmmRoot = document.getElementById('zmmtg-root');
   // const zmmRootContainer = document.getElementById('zmmtg-root-container');
   // $(zmmRootContainer).append(zmmRoot);
  }

  public async playVideoOnCard1(): Promise<void> {
    try {
      this.streamCard1 = await navigator.mediaDevices.getUserMedia(this.constraints);
      this.handleSuccess(this.streamCard1, 'video#card1');
    } catch (e) {
      this.handleError(e);
    }
  }

  stopCameraOnCard1(videoElId): void {
    const videoTracks = this.streamCard1.getVideoTracks();
    videoTracks.forEach((track) => {
      track.stop();
    });
    const video = document.querySelector(videoElId);
    video.srcObject = null;
  }

  public handleSuccess(stream: MediaStream, videoElId): void {
    const video = document.querySelector(videoElId);
    const videoTracks = stream.getVideoTracks();
    console.log('Got stream with constraints:', this.constraints);
    console.log(`Using video device: ${videoTracks[0].label}`);
    video.srcObject = stream;
  }

  public handleError(error: any): void {
    if (error.name === 'ConstraintNotSatisfiedError') {
      const v = this.constraints.video;
      this.errorMsg(`The resolution is not supported by your device.`);
    } else if (error.name === 'PermissionDeniedError') {
      this.errorMsg('Permissions have not been granted to use your camera and ' +
        'microphone, you need to allow the page access to your devices in ' +
        'order for the demo to work.');
    }
    this.errorMsg(`getUserMedia error: ${error.name}`, error);
  }

  public errorMsg(msg: string, error?: string): void {
    const errorElement = document.querySelector('#errorMsg');
    errorElement.innerHTML += `<p>${msg}</p>`;
    if (typeof error !== 'undefined') {
      console.error(error);
    }
  }

  onLoadIFrame(iframe: HTMLIFrameElement): void {
    console.log(iframe);
  }

  initMeeting(): void {
    const zoomMeeting = document.getElementById('zmmtg-root');
    const apiKey = '5fKKFBmsQPy4GxLlF-y0TA';
    const apiSecret = 'aL19k0BH0D8w4xbM04benBtVOKNH9iZBfEkZ';
    const meetingNumber = 3044848838;
    const role = 0;
    const password = 'WTJoLzBqUnQ2SFZTQXdJK2NaMjZCQT09';

    // const signature = this.generateSignature(apiKey, apiSecret, meetingNumber, role);

    const signaturedata = ZoomMtg.generateSignature({
      meetingNumber,
      apiKey,
      apiSecret,
      role,
      success: (res) => {
        console.log(res.result);
      }
    });

 /*   {

      meetingNumber: this.meetConfig.meetingNumber,
        apiKey: this.meetConfig.apiKey,
      apiSecret: this.meetConfig.apiSecret,
      role: this.meetConfig.role,
      success: (res) => {
      console.log(res.result);
    }
    }*/

    /*ZoomMtg.init({
      debug: true, //optional
      leaveUrl: 'http://www.zoom.us', //required
      webEndpoint: 'PSO web domain', // PSO option
      showMeetingHeader: false, //option
      disableInvite: false, //optional
      disableCallOut: false, //optional
      disableRecord: false, //optional
      disableJoinAudio: false, //optional
      audioPanelAlwaysOpen: true, //optional
      showPureSharingContent: false, //optional
      isSupportAV: true, //optional,
      isSupportChat: true, //optional,
      isSupportQA: true, //optional,
      isSupportCC: true, //optional,
      screenShare: true, //optional,
      rwcBackup: '', //optional,
      videoDrag: true, //optional,
      sharingMode: 'both', //optional,
      videoHeader: true, //optional,
      isLockBottom: true, // optional,
      isSupportNonverbal: true, // optional,
      isShowJoiningErrorDialog: true, // optional,
      inviteUrlFormat: '', // optional
      loginWindow: {  // optional,
        width: 400,
        height: 380
      },
      meetingInfo: [ // optional
        'topic',
        'host',
        'mn',
        'pwd',
        'telPwd',
        'invite',
        'participant',
        'dc'
      ],
      disableVoIP: false, // optional
      disableReport: false, // optional
    });*/

    /*ZoomMtg.join({
      meetingNumber: 123456789,
      userName: 'User name',
      userEmail: '',
      passWord: '',
      apiKey: 'API_KEY',
      signature: 'SIGNATURE',
      success: function(res){console.log(res)},
      error: function(res){console.log(res)}
    });*/

    ZoomMtg.join({
      signature: 'NWZLS0ZCbXNRUHk0R3hMbEYteTBUQS4zMDQ0ODQ4ODM4LjE2MDE4MzI2OTg3MzkuMC5FWlVVZHB5d0VSVExkcWhRUGNMUUF4MVpEYk51cGxUSmdNUFFIYmhiWFB3PQ==', // shorthand mode => signature: signature
      meetingNumber,
      userName: 'TESSA',
      apiKey,
      userEmail: 'tessa@tetra.team',
      passWord: password,
      success: (success) => {
        console.log(success);
      },
      error: (error) => {
        console.log(error);
      }
    });
  }

/*  generateSignature(apiKey, apiSecret, meetingNumber, role): any {
    // Prevent time sync issue between client signature generation and zoom
    const timestamp = new Date().getTime() - 30000;
    const msg = Buffer.from(apiKey + meetingNumber + timestamp + role).toString('base64');
    const hash = sha256(msg, apiSecret);
    const hashInBase64 = Base64.stringify(hash);
    return Buffer.from(`${apiKey}.${meetingNumber}.${timestamp}.${role}.${hashInBase64}`).toString('base64');
  }*/
  createMeeting(): void {
    const meetConfig = {
      apiKey: '5fKKFBmsQPy4GxLlF-y0TA',
      meetingNumber: '3044848838',
      leaveUrl: 'https://4c6021376233.ngrok.io/meetingEnd',
      userName: 'TESSA',
      userEmail: 'tessa@yoursite.com',
      passWord: 'WTJoLzBqUnQ2SFZTQXdJK2NaMjZCQT09', // if required
      role: 0 // 1 for host; 0 for attendee
    };

    this.http.post(
      'https://zoomjwtapp.herokuapp.com/',
      JSON.stringify({ meetingData: meetConfig })
    ).subscribe((resp: any) => {
      ZoomMtg.init({
        leaveUrl: meetConfig.leaveUrl,
        isSupportAV: true,
        success: (ss) => {
          console.log(ss);
          ZoomMtg.join({
            signature: resp.signature,
            apiKey: meetConfig.apiKey,
            meetingNumber: meetConfig.meetingNumber,
            userName: meetConfig.userName,
            // password optional; set by Host
            passWord: meetConfig.passWord,
            error(res): void {
              console.log(res);
            }
          });
        }
      });
    });
  }

  joinMeeting(): void {
    const meetConfig = {
      apiKey: '5fKKFBmsQPy4GxLlF-y0TA',
      meetingNumber: '3044848838',
      leaveUrl: 'https://4c6021376233.ngrok.io/web-rtc/zoom',
      userName: 'TESSA',
      userEmail: 'tessa@yoursite.com',
      passWord: 'WTJoLzBqUnQ2SFZTQXdJK2NaMjZCQT09', // if required
      role: 0 // 1 for host; 0 for attendee
    };

    this.http.post(this.signatureEndpoint, {
      meetingNumber: this.meetingNumber,
      role: this.role
    }).toPromise().then((data: any) => {
      if (data.signature) {
        console.log(data.signature);
        this.startMeeting(data.signature);
      } else {
        console.log(data);
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  private startMeeting(signature: any): void {
    const zmmRoot = document.getElementById('zmmtg-root');
    const zmmRootContainer = document.getElementById('zmmtg-root-container');
    $(zmmRootContainer).append(zmmRoot);
    document.getElementById('zmmtg-root').style.display = 'block';

    ZoomMtg.init({
      leaveUrl: this.leaveUrl,
      isSupportAV: true,
      success: (success) => {
        console.log(success);

        ZoomMtg.join({
          signature,
          meetingNumber: this.meetingNumber,
          userName: this.userName,
          apiKey: this.apiKey,
          userEmail: this.userEmail,
          passWord: this.passWord,
          success: (joinSuccess) => {
            console.log(joinSuccess);
          },
          error: (error) => {
            console.log(error);
          }
        });

      },
      error: (error) => {
        console.log(error);
      }
    });
  }

  startMirror(): void {
    const canvas: any = document.querySelector('canvas#main-video');
    const video: any = document.querySelector('video#zoom-mirror-video-el');

    video.srcObject = canvas.captureStream();
  }

  async getAV(): Promise<void> {
    try {
      const canvas: any = document.querySelector('canvas#main-video');
      const video: any = document.querySelector('video#zoom-mirror-video-el');
      const audio: any = document.querySelector('audio#zoom-mirror-audio-el');

      const stream = await navigator.mediaDevices.getUserMedia(this.constraints);
      const videoTracks = stream.getVideoTracks();
      const audioTracks = stream.getAudioTracks();

      console.log(videoTracks, audioTracks);

      audio.srcObject = stream;
      video.srcObject = stream;
      // video.srcObject = canvas.captureStream();

      console.log('Got stream with constraints:', this.constraints);
      console.log(`Using video device: ${videoTracks[0].label}`);
      console.log('Using audio device: ' + audioTracks[0].label);

    } catch (e) {
      this.handleError(e);
    }

  }
}
