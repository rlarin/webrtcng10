import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ZoomMtg } from '@zoomus/websdk';
import { ZoomService } from '../services/zoom.service';
import {environment} from '../../../../../environments/environment';
declare var $: any;
declare var MediaRecorder: any;
ZoomMtg.setZoomJSLib('https://source.zoom.us/1.8.1/lib', '/av');
ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

@Component({
  selector: 'app-join-zoom-meeting',
  templateUrl: './join-zoom-meeting.component.html',
  styleUrls: ['./join-zoom-meeting.component.less'],
})
export class JoinZoomMeetingComponent implements OnInit {
  public fg: FormGroup;
  public formSubmitted = false;
  // private mediaRecorder: any;
  private canvas: any;
  private stream: any;
  private recordedChunks = [];
  public recordButtonTextContent = 'Start Recording';
  public zoomViewTypeText = 'User View';
  constructor(
    private zoomService: ZoomService,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  public mediaRecorder: any;
  public recordedBlobs: any;
  public recordedVideoEl: any;
  public errorMsgElement: any;
  public gumVideoEl: any;
  public zoomCanvas: any;

  public constraints = {
    audio: {
      echoCancellation: { exact: false },
    },
    video: {
      width: 1280,
      height: 720,
    },
  };

  ngOnInit(): void {
    this.recordedVideoEl = document.querySelector('video#recorded');
    this.gumVideoEl = document.querySelector('video#gum');
    this.errorMsgElement = document.querySelector('#errorMsg');

    this.zoomCanvas = document.querySelector('canvas#main-video');
    // this.zoomCanvas = document.querySelector('.sharing-layout canvas');
    // const video: any = document.querySelector('video#zoom-mirror-video-el');

    ZoomMtg.inMeetingServiceListener('onMeetingStatus', (data) => {
      console.log('onMeetingStatus', data);
    });

    ZoomMtg.inMeetingServiceListener('initVideoEncode', (data) => {
      console.log('initVideoEncode', data);
    });

    this.watchActiveCanvas();
  }

  private createForm(): void {
    this.fg = this.fb.group({
      meetingNumber: [environment.meetConfig.meetingNumber, [Validators.required]],
      meetingPassWord: [
        environment.meetConfig.meetingPassword,
        [Validators.required],
      ],
      apiKey: [environment.meetConfig.apiKey, [Validators.required]],
      username: [environment.meetConfig.username, [Validators.required]],
      signatureEndpoint: [
        environment.meetConfig.signatureEndpoint,
        [Validators.required],
      ],
    });
  }

  joinMeeting(): void {
    this.log('Is data valid: ' + this.fg.valid);
    if (this.fg.valid) {
      const meetConfig = {
        apiKey: this.fg.get('apiKey').value || environment.meetConfig.apiKey,
        meetingNumber: this.fg.get('meetingNumber').value || environment.meetConfig.meetingNumber,
        leaveUrl: environment.meetConfig.leaveUrl,
        username: this.fg.get('username').value  || environment.meetConfig.username,
        signatureEndpoint: this.fg.get('signatureEndpoint').value  || environment.meetConfig.signatureEndpoint,
        userEmail: environment.meetConfig.userEmail,
        meetingPassword: this.fg.get('meetingPassword').value  || environment.meetConfig.meetingPassword, // if required
        role: environment.meetConfig.role, // 1 for host; 0 for attendee
      };

      this.zoomService
        .getSignature(meetConfig.signatureEndpoint, {
          meetingNumber: meetConfig.meetingNumber,
          role: meetConfig.role,
        })
        .subscribe(
          (data: any) => {
            if (data.signature) {
              this.log('Signature: ' + data.signature);
              this.startMeeting(data.signature, meetConfig);
            }
          },
          (e) => this.log(e)
        );
    }
  }

  private startMeeting(signature: any, meetConfig): void {
    const zmmRoot = document.getElementById('zmmtg-root');
    const zmmRootContainer = document.getElementById('zmmtg-root-container');
    $(zmmRootContainer).append(zmmRoot);
    document.getElementById('zmmtg-root').style.display = 'block';

    ZoomMtg.init({
      leaveUrl: meetConfig.leaveUrl,
      isSupportAV: true,
      success: (success) => {
        this.log(success);

        ZoomMtg.join({
          signature,
          meetingNumber: meetConfig.meetingNumber,
          userName: meetConfig.username,
          apiKey: meetConfig.apiKey,
          userEmail: meetConfig.userEmail,
          passWord: meetConfig.meetingPassword,
          success: (joinSuccess) => {
            this.log(joinSuccess);
          },
          error: (error) => {
            this.log(error);
          },
        });
      },
      error: (error) => {
        this.log(error);
      },
    });
  }

  private startVideoRecording(stream: any, lengthInMS: any): any {
    const recorder = new MediaRecorder(stream);
    const data = [];
    recorder.ondataavailable = (event) => data.push(event.data);
    recorder.start();
    console.log(recorder.state + ' for ' + lengthInMS / 1000 + ' seconds...');
    const stopped = new Promise((resolve, reject) => {
      recorder.onstop = resolve;
      recorder.onerror = (event) => reject(event.name);
    });
    setTimeout(
      () => recorder.state === 'recording' && recorder.stop(),
      lengthInMS
    );

    return Promise.all([
      stopped,
      // recorded
    ]).then(() => {
      console.log(data);
      return data;
    });
  }

  initRecording($event): void {
    const preview: any = document.getElementById('zoom-mirror-video-el');

    navigator.mediaDevices
      .getUserMedia({
        video: true,
        audio: true,
      })
      .then((stream) => {
        preview.srcObject = stream;
        preview.captureStream =
          preview.captureStream || preview.mozCaptureStream;
        return new Promise((resolve) => (preview.onplaying = resolve));
      })
      .then(() => this.startVideoRecording(preview.captureStream(), 5000))
      .then((recordedChunks) => {
        const recordedBlob = new Blob(recordedChunks, { type: 'video/webm' });
        /* recording.src = URL.createObjectURL(recordedBlob);
        downloadButton.href = recording.src;
        downloadButton.download = 'RecordedVideo.webm';*/
        console.log(
          'Successfully recorded ' +
            recordedBlob.size +
            ' bytes of ' +
            recordedBlob.type +
            ' media.'
        );
      });

    /* this.log('starting recording');
    this.canvas = document.querySelector('canvas#main-video');
    this.log('capturing stream');
    this.stream = this.canvas.captureStream(25);
    this.log('stream initialized');

    const options = { mimeType: 'video/webm; codecs=vp9' };
    this.mediaRecorder = new MediaRecorder(this.stream, options);
    this.log('mediaRecorder initialized');
    const data = [];
    // this.mediaRecorder.ondataavailable = this.handleDataAvailable.bind($event);
    this.mediaRecorder.ondataavailable = event => {
      console.log(event);
      data.push(event.data);
    };


    this.log('ondataavailable hook setup');
    this.mediaRecorder.start();
    this.log('media recording started'); /!**!/
    this.log(this.mediaRecorder.state + ' for ' + (this.mediaRecorder / 1000) + ' seconds...');*/
    /*const stopped = new Promise((resolve, reject) => {
      this.mediaRecorder.onstop = resolve;
      this.mediaRecorder.onerror = event => reject(event.name);
    });*/

    // let recorder = new MediaRecorder(stream);
    // // let data = [];
    // recorder.ondataavailable = event => data.push(event.data);
    // recorder.start();
    // log(recorder.state + " for " + (lengthInMS/1000) + " seconds...");
    // let stopped = new Promise((resolve, reject) => {
    //   recorder.onstop = resolve;
    //   recorder.onerror = event => reject(event.name);
    // });
    // let recorded = wait(lengthInMS).then(
    //   () => recorder.state == "recording" && recorder.stop()
    // );
    // return Promise.all([
    //   stopped,
    //   recorded
    // ])
    //   .then(() => data);
  }

  stopRecording(): void {
    this.log('stopping');
    this.mediaRecorder.stop();
    this.log('mediaRecorder stopped');
  }

  handleDataAvailable(event): void {
    console.log('handleDataAvailable', event);
    if (event.data && event.data.size > 0) {
      this.recordedBlobs.push(event.data);
    }
  }

  downloadRecording(event): void {
    const blob: any = new Blob(this.recordedBlobs, {
      type: 'video/webm',
    });
    const url: any = URL.createObjectURL(blob);
    const a: any = document.createElement('a');
    document.body.appendChild(a);
    a.style = 'display: none';
    a.href = url;
    a.download = 'test.webm';
    a.click();
    window.URL.revokeObjectURL(url);
  }

  log(msg: any): void {
    const logElement = document.getElementById('log');
    logElement.innerHTML += msg + '<br />';
  }

  play($event: MouseEvent): void {
    const superBuffer = new Blob(this.recordedBlobs, { type: 'video/webm' });
    this.recordedVideoEl.src = null;
    this.recordedVideoEl.srcObject = null;
    this.recordedVideoEl.src = window.URL.createObjectURL(superBuffer);
    this.recordedVideoEl.controls = true;
    this.recordedVideoEl.play();
  }

  async startCamera($event: MouseEvent): Promise<void> {
    await this.init(this.constraints);
  }

  async init(constraints): Promise<void> {
    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      this.handleSuccess(stream);
    } catch (e) {
      console.error('navigator.getUserMedia error:', e);
      this.errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
    }
  }

  handleSuccess(stream): void {
    // recordButton.disabled = false;
    console.log('getUserMedia() got stream:', stream);
    this.stream = stream;

    // const gumVideo = document.querySelector('video#gum');
    this.gumVideoEl.srcObject = stream;
  }

  handleRecordingBtnClick($event: MouseEvent, source: string): void {
    if (source === 'zoom') {
      this.zoomCanvas = document.querySelector('canvas#main-video');
      // this.zoomCanvas = document.querySelector('.sharing-layout canvas');
      console.log('.sharing-layout canvas');
      this.gumVideoEl.srcObject = this.zoomCanvas.captureStream();
      this.stream = this.zoomCanvas.captureStream();
    }

    if (this.recordButtonTextContent === 'Start Recording') {
      this.startRecordingNew($event);
    } else {
      this.stopRecording();
      this.recordButtonTextContent = 'Start Recording';
      // playButton.disabled = false;
      // downloadButton.disabled = false;
    }
  }

  startRecordingNew($event: MouseEvent): void {
    this.recordedBlobs = [];
    let options = { mimeType: 'video/webm;codecs=vp9,opus' };
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      console.error(`${options.mimeType} is not supported`);
      options = { mimeType: 'video/webm;codecs=vp8,opus' };
      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.error(`${options.mimeType} is not supported`);
        options = { mimeType: 'video/webm' };
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
          console.error(`${options.mimeType} is not supported`);
          options = { mimeType: '' };
        }
      }
    }

    try {
      this.mediaRecorder = new MediaRecorder(this.stream, options);
    } catch (e) {
      console.error('Exception while creating MediaRecorder:', e);
      this.errorMsgElement.innerHTML = `Exception while creating MediaRecorder: ${JSON.stringify(
        e
      )}`;
      return;
    }

    console.log(
      'Created MediaRecorder',
      this.mediaRecorder,
      'with options',
      options
    );
    this.recordButtonTextContent = 'Stop Recording';
    // playButton.disabled = true;
    // downloadButton.disabled = true;
    this.mediaRecorder.onstop = (event) => {
      console.log('Recorder stopped: ', event);
      console.log('Recorded Blobs: ', this.recordedBlobs);
    };
    this.mediaRecorder.ondataavailable = this.handleDataAvailable.bind(this);
    this.mediaRecorder.start();
    console.log('MediaRecorder started', this.mediaRecorder);
  }

  playRecording($event: MouseEvent): void {
    const superBuffer = new Blob(this.recordedBlobs, { type: 'video/webm' });
    this.recordedVideoEl.src = null;
    this.recordedVideoEl.srcObject = null;
    this.recordedVideoEl.src = window.URL.createObjectURL(superBuffer);
    this.recordedVideoEl.controls = true;
    this.recordedVideoEl.play();
  }

  private watchActiveCanvas(): void {
    setInterval(() => {

    });
  }

}
