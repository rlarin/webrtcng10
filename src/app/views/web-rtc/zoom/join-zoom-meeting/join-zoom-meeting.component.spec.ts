import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinZoomMeetingComponent } from './join-zoom-meeting.component';

describe('JoinZoomMeetingComponent', () => {
  let component: JoinZoomMeetingComponent;
  let fixture: ComponentFixture<JoinZoomMeetingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoinZoomMeetingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinZoomMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
