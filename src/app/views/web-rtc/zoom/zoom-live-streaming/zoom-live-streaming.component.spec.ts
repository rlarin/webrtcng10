import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomLiveStreamingComponent } from './zoom-live-streaming.component';

describe('ZoomLiveStreamingComponent', () => {
  let component: ZoomLiveStreamingComponent;
  let fixture: ComponentFixture<ZoomLiveStreamingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoomLiveStreamingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomLiveStreamingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
