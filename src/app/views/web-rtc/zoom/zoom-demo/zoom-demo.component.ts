import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  IActiveCanvasData,
  ILayoutsOfInterest,
  RECORDING_CLASS_TYPE,
  RECORDING_MODE,
  TResult,
  ZOOM_ACTIONS,
  ZoomService
} from '../services/zoom.service';
import {environment} from '../../../../../environments/environment';
import {ZoomMtg} from '@zoomus/websdk';
import {Subscription} from 'rxjs';
ZoomMtg.setZoomJSLib('https://source.zoom.us/1.8.1/lib', '/av');
ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

declare var RecordRTC: any;
declare var $: any;
declare var ConcatenateBlobs: any;
declare var StereoAudioRecorder: any;
declare var MediaStreamRecorder: any;
declare var html2canvas: any;
declare var bytesToSize: any;

@Component({
  selector: 'app-zoom-demo',
  templateUrl: './zoom-demo.component.html',
  styleUrls: ['./zoom-demo.component.less']
})
export class ZoomDemoComponent implements OnInit, OnDestroy {
  public fg: FormGroup;
  public formSubmitted = false;
  // public isRecording = false;
  public isStopping = false;
  public isHtml2canvasWorking = false;
  public zoomViewTypeText = 'User View';
  private videoRecorder: any;
  public mediaDevices: any[] = [];
  public zoomActiveCanvasDataSub: Subscription;
  public actionDataSub: Subscription;

  // video
  public currentActiveCanvasData: IActiveCanvasData = {
    el: null,
    recordingMode: RECORDING_MODE.NONE,
    recordingClassType: RECORDING_CLASS_TYPE.NONE,
    reset(): void {
      this.el = null;
      this.recordingMode = RECORDING_MODE.NONE;
    }
  };
  public prevActiveCanvasData: IActiveCanvasData;
  public layoutOfInterest: ILayoutsOfInterest[] = [{
    recordingClassType: RECORDING_CLASS_TYPE.SHARED_SCREEN,
    recordingMode: RECORDING_MODE.NONE
  }, {
    recordingClassType: RECORDING_CLASS_TYPE.MAIN_VIDEO,
    recordingMode: RECORDING_MODE.NONE
  }];
  public arrayOfRecordedBlobs: any[] = [];
  private recordedBlobsConcatenated: any;
  private HTMLContainerEl = '.active-video-container';
  public elementToRecord: any;
  public canvas2d: any;
  public context: any;
  public htmlLooperSI: any;
  public blobSizeText = '';
  public html2canvasIsRecording = false;
  private videoControlEl: any;
  private videoControlElMirror: any;

  // Audio
  private audioRecorder: any;
  private audioControlEl: any;
  private audioInput: any;
  private isEdge: any;
  private isSafari: any;
  private rtcOptions = {
    type: 'canvas',
    mimeType: 'video/x-matroska;codecs=avc1',
  };

  constructor(
    public zoomService: ZoomService,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.isEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob);
    this.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    this.audioControlEl = document.querySelector('audio#audioControlEl');
    this.videoControlEl = document.querySelector('video#videoControlEl');
    this.videoControlElMirror = document.querySelector('video#videoControlElMirror');
    this.canvas2d = document.getElementById('background-canvas');

    this.zoomActiveCanvasDataSub =
      this.zoomService.zoomActiveCanvasDataSubject
        .subscribe((r: TResult<IActiveCanvasData>) => {
          console.log(this.currentActiveCanvasData.recordingClassType, this.currentActiveCanvasData.recordingMode);
          this.zoomViewTypeText = this.currentActiveCanvasData.recordingClassType + ' - ' + this.currentActiveCanvasData.recordingMode;
          if (this.zoomService.isRecording) {
            this.recordElementChanged();
          }

          this.ref.detectChanges();
        });

    this.zoomService.actionDataSubject
      .subscribe((r: TResult<any>) => {
        if (r.success) {
          switch (r.data.triggerAction) {
            case 'HANDLE_RECORDING':
              this.handleRecordingBtnClick(r.data.params.eventData);
              break;

            case 'DOWNLOAD_VIDEO':
              this.downloadVideoRecording(r.data.params.eventData);
              break;

            case 'DOWNLOAD_AUDIO':
              this.downloadAudioRecording(r.data.params.eventData);
              break;
          }
        }

      });

    this.getAvailableMediaDevices();


    this.subscribeToZoomEvents();
  }

  ngOnDestroy(): void {
    this.zoomActiveCanvasDataSub.unsubscribe();
    this.actionDataSub.unsubscribe();
  }

  private createForm(): void {
    this.fg = this.fb.group({
      meetingLink: [environment.meetConfig.meetingLink],
      meetingNumber: [environment.meetConfig.meetingNumber, [Validators.required]],
      meetingPassword: [
        environment.meetConfig.meetingPassword,
        [Validators.required],
      ],
      apiKey: [environment.meetConfig.apiKey, [Validators.required]],
      username: [environment.meetConfig.username, [Validators.required]],
      signatureEndpoint: [
        environment.meetConfig.signatureEndpoint,
        [Validators.required],
      ],
    });
  }

  joinZoomMeeting(): void {
    if (this.fg.valid) {
      const meetConfig = {
        apiKey: this.fg.get('apiKey').value || environment.meetConfig.apiKey,
        meetingNumber: this.fg.get('meetingNumber').value || environment.meetConfig.meetingNumber,
        leaveUrl: environment.meetConfig.leaveUrl,
        username: this.fg.get('username').value  || environment.meetConfig.username,
        signatureEndpoint: this.fg.get('signatureEndpoint').value  || environment.meetConfig.signatureEndpoint,
        userEmail: environment.meetConfig.userEmail,
        meetingPassword: this.fg.get('meetingPassword').value  || environment.meetConfig.meetingPassword, // if required
        role: environment.meetConfig.role, // 1 for host; 0 for attendee
      };


      this.zoomService
        .getSignature(meetConfig.signatureEndpoint, {
          meetingNumber: meetConfig.meetingNumber,
          role: meetConfig.role,
        }).subscribe(
        (data: any) => {
          if (data.signature) {
            console.log('Signature: ' + data.signature);
            this.startMeeting(data.signature, meetConfig);
          }
        },
        (e) => console.log(e)
      );
    }
  }

  private startMeeting(signature: any, meetConfig: any): any {
    const zmmRoot = document.getElementById('zmmtg-root');
    const zmmRootContainer = document.getElementById('zmmtg-root-container');
    $(zmmRootContainer).append(zmmRoot);
    document.getElementById('zmmtg-root').style.display = 'block';

    ZoomMtg.init({
      leaveUrl: meetConfig.leaveUrl,
      isSupportAV: true,
      success: (success) => {
        console.log(success);

        ZoomMtg.join({
          signature,
          meetingNumber: meetConfig.meetingNumber,
          userName: meetConfig.username,
          apiKey: meetConfig.apiKey,
          userEmail: meetConfig.userEmail,
          passWord: meetConfig.meetingPassword,
          success: (joinSuccess) => {
            console.log(joinSuccess);
            // this.activeCanvasWatcher();
          },
          error: (error) => {
            console.log(error);
          },
        });
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  handleRecordingBtnClick($event: MouseEvent): void {
    if (!this.zoomService.isRecording) {
      this.resetView();

      this.zoomService.isRecording = true;
      this.startRecordingAudio();
      this.activateVideoElementWatcher();
    } else {
      this.zoomService.isRecording = false;
      this.stopRecording();
    }
  }

  private startRecordingAudio(): void {
    if (!this.audioInput) {
      this.captureAudioInput((mediaStream) => {
        this.audioInput = mediaStream;
        const options = {
          type: 'audio',
          numberOfAudioChannels: this.isEdge ? 1 : 2,
          checkForInactiveTracks: true,
          bufferSize: 16384,
          recorderType: null,
          sampleRate: null,
        };

        if (this.isSafari || this.isEdge) {
          options.recorderType = StereoAudioRecorder;
        }

        if (navigator.platform && navigator.platform.toString().toLowerCase().indexOf('win') === -1) {
          options.sampleRate = 48000; // or 44100 or remove this line for default
        }

        this.audioRecorder = RecordRTC(this.audioInput, options);

        this.audioRecorder.startRecording();
      });
    }
  }

  private captureAudioInput(callback: (mediaStream) => void): void {
    if (this.audioInput) {
      callback(this.audioInput);
      return;
    }

    if (typeof navigator.mediaDevices === 'undefined' || !navigator.mediaDevices.getUserMedia) {
      alert('This browser does not supports WebRTC getUserMedia API.');

      if (!!navigator.getUserMedia) {
        alert('This browser seems supporting deprecated getUserMedia API.');
      }
    }

    navigator.mediaDevices.getUserMedia({
      audio: this.isEdge ? true : {
        echoCancellation: false
      }
    }).then((mediaStream) => {
      callback(mediaStream);
    }).catch((error) => {
      alert('Unable to capture your microphone. Please check console logs.');
      console.error(error);
    });
  }

  private activateVideoElementWatcher(): void {
    setInterval(() => {
      if (!this.isStopping) {
        this.prevActiveCanvasData = {...this.currentActiveCanvasData};
        this.currentActiveCanvasData.reset();

        for (const lEl of this.layoutOfInterest) {
          const $lEl = $(lEl.recordingClassType);
          if ($lEl.css('display') === 'block') {
            const canvas = $lEl.find('canvas:visible').get(0);
            if (canvas) {
              this.currentActiveCanvasData.el = canvas;
              this.currentActiveCanvasData.recordingClassType = lEl.recordingClassType;
              this.currentActiveCanvasData.recordingMode = RECORDING_MODE.CANVAS;
            } else {
              this.currentActiveCanvasData.el = $lEl.find(this.HTMLContainerEl).get(0);
              this.currentActiveCanvasData.recordingClassType = lEl.recordingClassType;
              this.currentActiveCanvasData.recordingMode = RECORDING_MODE.HTML_ELEMENT;
            }
          }
        }

        if (
          (this.prevActiveCanvasData.recordingMode !== this.currentActiveCanvasData.recordingMode) ||
          (this.prevActiveCanvasData.recordingClassType !== this.currentActiveCanvasData.recordingClassType)
        ) {
          this.zoomService.zoomActiveCanvasDataSubject.next(
            new TResult<IActiveCanvasData>(
              true,
              ZOOM_ACTIONS.RECORDING_MODE_Changed,
              this.currentActiveCanvasData
            )
          );
        }
      }
    }, 200);
  }

  private stopRecordingCallback(recorder): void{
    const blob = recorder.getBlob();
    this.arrayOfRecordedBlobs.push(blob);
    this.ref.detectChanges();
    recorder = null;
    if (this.zoomService.isRecording) {
      this.restartVideoRecording(this.currentActiveCanvasData.recordingMode);
    }
  }

  private recordElementChanged(): void {
    if (this.zoomService.isRecording) {
      if (this.videoRecorder) {
        // this.videoRecorder.stopRecording(this.stopRecordingCallback.bind(this)(this.videoRecorder));
        return this.stopRecording(false);
      } else {
        this.restartVideoRecording(this.currentActiveCanvasData.recordingMode);
      }
    }
  }

  private restartVideoRecording(recordingMode): void {
    console.log('restarting Video Recording...');
    switch (recordingMode) {
      case RECORDING_MODE.CANVAS :
        this.startCanvasRecording();
        break;
      case RECORDING_MODE.HTML_ELEMENT :
        this.startHTMLRecording();
        break;
    }
  }

  private startCanvasRecording(): void {
    this.videoRecorder = new RecordRTC(this.currentActiveCanvasData.el, this.rtcOptions);
    // this.InternalRecorderLooper();
    this.videoRecorder.startRecording();
    this.ref.detectChanges();
  }

  private startHTMLRecording(): void {
    this.elementToRecord = this.currentActiveCanvasData.el;
    this.canvas2d = document.getElementById('background-canvas');
    this.context = this.canvas2d.getContext('2d');
    this.canvas2d.width = this.elementToRecord.clientWidth;
    this.canvas2d.height = this.elementToRecord.clientHeight;
    this.videoRecorder = new RecordRTC(this.canvas2d,  this.rtcOptions);
    this.htmlLooper();
    this.videoRecorder.startRecording();
    this.html2canvasIsRecording = true;
    this.ref.detectChanges();
  }

  private htmlLooper(): void {
    this.htmlLooperSI = setInterval(() => {
      if (!this.isHtml2canvasWorking) {
        this.isHtml2canvasWorking = true;
        html2canvas(this.elementToRecord).then((canvas) => {
          if ((this.canvas2d.width > 0) && (this.canvas2d.height > 0)) {
            this.isHtml2canvasWorking = false;
            this.context.clearRect(0, 0, this.canvas2d.width, this.canvas2d.height);
            this.context.drawImage(canvas, 0, 0, this.canvas2d.width, this.canvas2d.height);
            console.log('HTML recording...');
            if (!this.html2canvasIsRecording) {
              console.log('STOP HTML recording');
              return;
            }

            requestAnimationFrame(this.htmlLooper.bind(this));
          }
        });
      }
      clearInterval(this.htmlLooperSI);
      this.htmlLooperSI = null;
    }, 10);
  }

  private stopRecording(canMerge = true): void {
    if (this.videoRecorder) {
      this.isStopping = true;
      this.videoRecorder.stopRecording(() => {
        this.html2canvasIsRecording = false;
        this.isStopping = false;
        const blob = this.videoRecorder.getBlob();
        this.arrayOfRecordedBlobs.push(blob);

        if (this.zoomService.isRecording) {
          setTimeout(() => {
            this.restartVideoRecording(this.currentActiveCanvasData.recordingMode);
          }, 200);
        }

        if (canMerge) {
          this.handleBlobsMerge();
        }
      });
    }

    if (this.audioRecorder) {
      this.audioRecorder.stopRecording();
    }
  }

  private handleBlobsMerge(): void {
    console.log('Saving Audio...');
    this.audioRecorder.stopRecording(this.stopAudioRecordingCallback.bind(this));

    console.log('concatenating blobs...');
    this.recordedBlobsConcatenated =
      this.arrayOfRecordedBlobs.reduce(
        (accumulator, currentValue) => new Blob([accumulator, currentValue], {type: 'video/webm; codecs=vorbis,vp8'})
      );

    // this.blobSizeText = '| Recording length: ' + bytesToSize(this.recordedBlobsConcatenated.size);
    console.log('concatenating blobs done!');

    this.destroyAllRecorders();
  }

  private stopAudioRecordingCallback(): void {
    // const blob = this.audioRecorder.getBlob();
    // this.audioControlEl.src = URL.createObjectURL(blob);
    // this.audioControlEl.play();
    /*setTimeout(() => {
      this.downloadBlob(blob, 'tessa-audio.webm');
    }, 200);*/
  }

  downloadBlob(blob: Blob, filename: string): void {
    console.log('createObjectURL');
    const url: any = URL.createObjectURL(blob);
    const a: any = document.createElement('a');
    document.body.appendChild(a);
    a.style = 'display: none';
    a.href = url;
    // a.download = 'tessa-audio.webm';
    a.download = filename;
    a.click();
    window.URL.revokeObjectURL(url);
  }

  downloadRecording($event: MouseEvent): void {
    this.downloadAudioRecording($event);
    this.downloadVideoRecording($event);
  }

  downloadVideoRecording($event: MouseEvent): void {
    // download video blobs
    if (this.arrayOfRecordedBlobs.length > 0) {
      const blobType = this.arrayOfRecordedBlobs[0].type;
      ConcatenateBlobs(this.arrayOfRecordedBlobs, blobType, (resultingBlob) => {
        this.downloadBlob(resultingBlob, 'tessa-video.webm');
      });
    } else {
      console.log('No video blobs available!');
    }
  }

  downloadAudioRecording($event: MouseEvent): void {
    // download audio blob
    const blob = this.audioRecorder.getBlob();
    this.downloadBlob(blob, 'tessa-audio.webm');
  }

  playRecording($event: MouseEvent): void {
    const audioBlob = (this.audioRecorder) ? this.audioRecorder.getBlob() : undefined;
    if (audioBlob) {
      this.audioControlEl.src = URL.createObjectURL(audioBlob);
      this.audioControlEl.play();
    } else { console.log('No audio recorded'); }


    const videoBlob = this.recordedBlobsConcatenated;
    if (videoBlob) {
      this.videoControlEl.src = URL.createObjectURL(videoBlob);
      this.videoControlEl.play();
    } else { console.log('No video recorded'); }
  }

  /*private InternalRecorderLooper(): void {
    this.internalRecorderLooperSI = setInterval(() => {
      const internal = this.videoRecorder.getInternalRecorder();
      if (internal && internal.getArrayOfBlobs) {
        const blob = new Blob(internal.getArrayOfBlobs(), {
          type: 'video/webm'
        });

        console.log('Recording length: ' + bytesToSize(blob.size));
        document.querySelector('h1').innerHTML = 'Recording length: ' + bytesToSize(blob.size);
        if (!this.zoomService.isRecording) {
         clearInterval(this.internalRecorderLooperSI);
        }
      }
    }, 200);
  }*/
  private resetView(): void {
    this.arrayOfRecordedBlobs = []; // reset arrayOfRecordedBlobs
    this.audioControlEl.pause();
    this.audioControlEl.removeAttribute('src'); // empty source
    this.audioControlEl.load();

    this.videoControlEl.pause();
    this.videoControlEl.removeAttribute('src'); // empty source
    this.videoControlEl.load();

    this.isStopping = false;
    if (this.prevActiveCanvasData) {
      this.prevActiveCanvasData.reset();
    }

    if (this.currentActiveCanvasData) {
      this.currentActiveCanvasData.reset();
    }
  }

  setMeetingData($event: MouseEvent): void {
    const zoomLink = this.fg.get('meetingLink').value
      .replace(/%3D/, '=')
      .match(/[https\:\/\/zoom\.us\/j\/]+[0-9]+\?pwd=\w+/)[0];

    const meetingId = zoomLink.match(/\/[0-9]+/gm)[0].substr(1);
    const meetingPassword = zoomLink.match(/\?pwd=\w+/gm)[0].substr(5);
    console.log(meetingId, meetingPassword);
    this.fg.patchValue({
      meetingNumber: meetingId,
      meetingPassword
    });
  }

  private destroyAllRecorders(): void {

  }

  private getAvailableMediaDevices(): void {
    navigator.mediaDevices.enumerateDevices()
      .then((devices) => {
        console.log('devices: ', devices);
        /*  const filteredDevices = devices.filter(
            (v, i, a) => a.findIndex(t => (t.groupId === v.groupId)) === i
          );*/
        this.mediaDevices = [...devices];
      });
  }

  private subscribeToZoomEvents(): void {
    console.log('subscribeToZoomEvents...');
    // status: 1 ( connecting )
    // status: 2 (connected)
    // status: 3 (disconnected)
    ZoomMtg.inMeetingServiceListener('onMeetingStatus', (status) => {

      console.log('onMeetingStatus: ', status);
    });
  }
}
