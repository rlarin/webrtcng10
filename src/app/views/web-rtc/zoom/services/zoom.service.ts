import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';

export enum ZOOM_ACTIONS {
  RECORDING_MODE_Changed = 'RECORDING_MODE_Changed',
  GENERIC = 'GENERIC',
}

export class TResult<T> {
  public id: string;
  constructor(
    public success: boolean,
    public action: ZOOM_ACTIONS,
    public data?: T,
  ) { }
}

export enum RECORDING_MODE {
  NONE = 'NONE',
  CANVAS = 'CANVAS',
  HTML_ELEMENT = 'HTML_ELEMENT'
}

export enum RECORDING_CLASS_TYPE {
  NONE = 'NONE',
  MAIN_VIDEO = '.main-layout',
  SHARED_SCREEN = '.sharing-layout'
}



export interface ILayoutsOfInterest {
  recordingMode: RECORDING_MODE;
  recordingClassType: RECORDING_CLASS_TYPE;
}

export interface IActiveCanvasData {
  el: any;
  recordingMode: RECORDING_MODE;
  recordingClassType: RECORDING_CLASS_TYPE;
  reset(): void;
}

@Injectable({
  providedIn: 'root'
})
export class ZoomService {
  public zoomActiveCanvasDataSubject = new Subject<TResult<IActiveCanvasData>>();
  public actionDataSubject = new Subject<TResult<any>>();
  public isRecording = false;

  constructor(
    public http: HttpClient,
    ) { }

  getSignature(signatureEndpoint: any, data: { meetingNumber: any; role: number }): Observable<any> {
    return this.http.post(signatureEndpoint, data);
  }
}
