import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoomMeetingRecorderComponent } from './zoom-meeting-recorder.component';

describe('ZoomMeetingRecorderComponent', () => {
  let component: ZoomMeetingRecorderComponent;
  let fixture: ComponentFixture<ZoomMeetingRecorderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoomMeetingRecorderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoomMeetingRecorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
