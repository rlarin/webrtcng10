import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ZoomMtg} from '@zoomus/websdk';
import {
  IActiveCanvasData,
  ILayoutsOfInterest,
  RECORDING_CLASS_TYPE,
  RECORDING_MODE,
  TResult,
  ZOOM_ACTIONS,
  ZoomService
} from '../services/zoom.service';
import {environment} from '../../../../../environments/environment';

declare var $: any;
declare var MediaRecorder: any;
declare var StereoAudioRecorder: any;
declare var RecordRTC: any;
declare var CanvasRecorder: any;
declare var html2canvas: any;
declare var ConcatenateBlobs: any;

ZoomMtg.setZoomJSLib('https://source.zoom.us/1.8.1/lib', '/av');
ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();




@Component({
  selector: 'app-zoom-meeting-recorder',
  templateUrl: './zoom-meeting-recorder.component.html',
  styleUrls: ['./zoom-meeting-recorder.component.less']
})
export class ZoomMeetingRecorderComponent implements OnInit {

  public fg: FormGroup;
  public formSubmitted = false;
  public zoomViewTypeText = 'User View';
  public recordButtonTextContent = 'Start Recording';
  public stream: any;
  public layoutOfInterest: ILayoutsOfInterest[] = [{
    recordingClassType: RECORDING_CLASS_TYPE.SHARED_SCREEN,
    recordingMode: RECORDING_MODE.NONE
  }, {
    recordingClassType: RECORDING_CLASS_TYPE.MAIN_VIDEO,
    recordingMode: RECORDING_MODE.NONE
  }];
  public currentActiveCanvasData: IActiveCanvasData = {
    el: null,
    recordingMode: RECORDING_MODE.NONE,
    recordingClassType: RECORDING_CLASS_TYPE.NONE,
    reset(): void {
      this.el = null;
      this.recordingMode = RECORDING_MODE.NONE;
    }
  };
  public prevActiveCanvasData: IActiveCanvasData;
  // public recordedBlobs: any[] = [];
  public arrayOfRecordedBlobs: any[] = [];
  private recordedBlobsConcatenated: any;
  private mediaRecorder: any;

  private microphone: any;
  private recorder: any;
  private isEdge: any;
  private isSafari: any;
  private audioRecorded: any;



  public elementToRecord: any;
  public canvas2d: any;
  public context: any;
  public htmlCanvasRecorder: any;
  public htmlLooperSI: any;
  public html2canvasIsRecording = false;
  public isRecording = false;
  private HTMLContainerEl = '.active-video-container__avatar';
  currentRecordingText = 'Recording inactive';

  constructor(
    private zoomService: ZoomService,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    // this.currentActiveCanvasEl = document.querySelector('canvas#main-video');
    this.isEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob);
    this.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    this.audioRecorded = document.querySelector('audio#audioRecorded');

    // this.elementToRecord = document.querySelector('.active-video-container');
    this.canvas2d = document.getElementById('background-canvas');
    this.context = this.canvas2d.getContext('2d');

    this.zoomService.zoomActiveCanvasDataSubject
      .subscribe((r: TResult<IActiveCanvasData>) => {
        console.log(this.currentActiveCanvasData.recordingClassType, this.currentActiveCanvasData.recordingMode);
        this.zoomViewTypeText = this.currentActiveCanvasData.recordingClassType + ' - ' + this.currentActiveCanvasData.recordingMode;
        if (this.isRecording) {
          this.recordElementChanged();
        }

        this.ref.detectChanges();
      });
  }

  joinMeeting(): void {
    if (this.fg.valid) {
      const meetConfig = {
        apiKey: this.fg.get('apiKey').value || environment.meetConfig.apiKey,
        meetingNumber: this.fg.get('meetingNumber').value || environment.meetConfig.meetingNumber,
        leaveUrl: environment.meetConfig.leaveUrl,
        username: this.fg.get('username').value  || environment.meetConfig.username,
        signatureEndpoint: this.fg.get('signatureEndpoint').value  || environment.meetConfig.signatureEndpoint,
        userEmail: environment.meetConfig.userEmail,
        meetingPassword: this.fg.get('meetingPassword').value  || environment.meetConfig.meetingPassword, // if required
        role: environment.meetConfig.role, // 1 for host; 0 for attendee
      };


      this.zoomService
        .getSignature(meetConfig.signatureEndpoint, {
          meetingNumber: meetConfig.meetingNumber,
          role: meetConfig.role,
        }).subscribe(
        (data: any) => {
          if (data.signature) {
            console.log('Signature: ' + data.signature);
            this.startMeeting(data.signature, meetConfig);
          }
        },
        (e) => console.log(e)
      );
    }
  }

  private createForm(): void {
    this.fg = this.fb.group({
      meetingNumber: [environment.meetConfig.meetingNumber, [Validators.required]],
      meetingPassword: [
        environment.meetConfig.meetingPassword,
        [Validators.required],
      ],
      apiKey: [environment.meetConfig.apiKey, [Validators.required]],
      username: [environment.meetConfig.username, [Validators.required]],
      signatureEndpoint: [
        environment.meetConfig.signatureEndpoint,
        [Validators.required],
      ],
    });
  }

  handleRecordingBtnClick($event: MouseEvent): void {
    if (!this.isRecording) {
      this.arrayOfRecordedBlobs = []; // reset arrayOfRecordedBlobs
      this.isRecording = true;
      this.startRecordingAudio();
      this.activeCanvasWatcher();
      // this.recordElementChanged();
    } else {
      this.isRecording = false;
      this.stopRecording(this.prevActiveCanvasData.recordingMode, !!($event));
      this.recordButtonTextContent = 'Start Recording';
    }
  }

  playRecording($event: MouseEvent): void {
    // const superBuffer = new Blob(this.recordedBlobs, { type: 'video/webm' });
    // const superBuffer = new Blob(this.arrayOfRecordedBlobs, { type: 'video/webm' });
    /*const superBuffer = await this.concatenateBlobs(
      this.arrayOfRecordedBlobs,
      this.arrayOfRecordedBlobs[0].type
    );
    console.log(superBuffer);*/

    // const sceneBlob = new Blob(scenes[sceneNum], {type: 'video/webm; codecs=vorbis,vp8'});
    // vid.src = URL.createObjectURL(sceneBlob);

    this.recordedBlobsConcatenated = this.arrayOfRecordedBlobs.reduce((a, b) => new Blob([a, b], {type: 'video/webm; codecs=vorbis,vp8'}));


    $('#ZMR_PLAYER').css('display', 'block');
    const zmrPlayer: any = document.querySelector('#ZMR_PLAYER video');
    zmrPlayer.src = null;
    zmrPlayer.srcObject = null;
    zmrPlayer.src = window.URL.createObjectURL(this.recordedBlobsConcatenated);
    zmrPlayer.controls = true;
    zmrPlayer.play();
  }

  downloadRecording($event: MouseEvent): void {
    /*const blob: any = new Blob(this.recordedBlobs, {
      type: 'video/webm',
    });*/
    /*const blob: any = new Blob(this.arrayOfRecordedBlobs);
    const url: any = URL.createObjectURL(blob);
    const a: any = document.createElement('a');
    document.body.appendChild(a);
    a.style = 'display: none';
    a.href = url;
    a.download = 'testV/test.webm';
    a.click();
    window.URL.revokeObjectURL(url);*/

    this.downloadAudioRecording(this.recorder.getBlob());

    const blobType = this.arrayOfRecordedBlobs[0].type;


    ConcatenateBlobs(this.arrayOfRecordedBlobs, blobType, (resultingBlob) => {
      const url: any = URL.createObjectURL(resultingBlob);
      const a: any = document.createElement('a');
      document.body.appendChild(a);
      a.style = 'display: none';
      a.href = url;
      a.download = 'tessa-video.webm';
      a.click();
      window.URL.revokeObjectURL(url);
    });
  }

  downloadAudioRecording(blob: Blob): void {
    const url: any = URL.createObjectURL(blob);
    const a: any = document.createElement('a');
    document.body.appendChild(a);
    a.style = 'display: none';
    a.href = url;
    a.download = 'tessa-audio.webm';
    a.click();
    window.URL.revokeObjectURL(url);
  }

  handleDataAvailable(event): void {
    // console.log('handleDataAvailable', event);
    if (event.data && event.data.size > 0) {
      this.arrayOfRecordedBlobs.push(event.data);
    }
  }

  private startMeeting(signature: any, meetConfig: any): any {
    const zmmRoot = document.getElementById('zmmtg-root');
    const zmmRootContainer = document.getElementById('zmmtg-root-container');
    $(zmmRootContainer).append(zmmRoot);
    document.getElementById('zmmtg-root').style.display = 'block';

    ZoomMtg.init({
      leaveUrl: meetConfig.leaveUrl,
      isSupportAV: true,
      success: (success) => {
        console.log(success);

        ZoomMtg.join({
          signature,
          meetingNumber: meetConfig.meetingNumber,
          userName: meetConfig.username,
          apiKey: meetConfig.apiKey,
          userEmail: meetConfig.userEmail,
          passWord: meetConfig.meetingPassword,
          success: (joinSuccess) => {
            console.log(joinSuccess);
            // this.activeCanvasWatcher();
          },
          error: (error) => {
            console.log(error);
          },
        });
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  private showPlayer(stream): void {
    $('#ZMR_PLAYER').css('display', 'block');
    const zmrPlayer: any = document.querySelector('#ZMR_PLAYER video');
    // zmrPlayer.srcObject = this.currentActiveCanvasData.el.captureStream();
    zmrPlayer.src = null;
    zmrPlayer.srcObject = null;
    zmrPlayer.srcObject = stream;
    zmrPlayer.controls = true;
    zmrPlayer.play();
  }

  activeCanvasWatcher(): void {
    setInterval(() => {
      // const canvases: any = document.querySelectorAll('.meeting-app canvas');
      this.prevActiveCanvasData = {...this.currentActiveCanvasData};
      this.currentActiveCanvasData.reset();
      for (const lEl of this.layoutOfInterest) {
        const $lEl = $(lEl.recordingClassType);
        if ($lEl.css('display') === 'block') {
          const canvas = $lEl.find('canvas:visible').get(0);
          if (canvas) {
            this.currentActiveCanvasData.el = canvas;
            this.currentActiveCanvasData.recordingClassType = lEl.recordingClassType;
            this.currentActiveCanvasData.recordingMode = RECORDING_MODE.CANVAS;
          } else {
            this.currentActiveCanvasData.el = $lEl.find(this.HTMLContainerEl).get(0);
            this.currentActiveCanvasData.recordingClassType = lEl.recordingClassType;
            this.currentActiveCanvasData.recordingMode = RECORDING_MODE.HTML_ELEMENT;
          }
        }
      }

      if (
        (this.prevActiveCanvasData.recordingMode !== this.currentActiveCanvasData.recordingMode) ||
        (this.prevActiveCanvasData.recordingClassType !== this.currentActiveCanvasData.recordingClassType)
      ) {
        this.zoomService.zoomActiveCanvasDataSubject.next(
          new TResult<IActiveCanvasData>(
            true,
            ZOOM_ACTIONS.RECORDING_MODE_Changed,
            this.currentActiveCanvasData
          )
        );
      }
    }, 200);


    /*setInterval(() => {
      const mainVideoCanvas: any = document.querySelector('canvas#main-video');
      const sharingLayout: any = document.querySelector('.sharing-layout');
      const sharingLayoutCanvas: any = document.querySelector('.sharing-layout canvas');

      if (mainVideoCanvas || sharingLayoutCanvas) {
        if ((mainVideoCanvas.style.display !== 'none')) {
          this.currentActiveCanvasData.el = mainVideoCanvas;

          if (this.currentActiveCanvasData.isSharingScreen) {
            this.currentActiveCanvasData.isSharingScreen = false;
            this.restartRecording();
          }
        } else if ((sharingLayout.style.display !== 'none')) {
          this.currentActiveCanvasData.el = sharingLayoutCanvas;

          if (!this.currentActiveCanvasData.isSharingScreen) {
            this.currentActiveCanvasData.isSharingScreen = true;
            this.restartRecording();
          }
        }

        if (this.currentActiveCanvasData.el) {
          // console.log('Canvas El: ' + ((!this.currentActiveCanvasData.isSharingScreen) ? 'Main-video' : 'Sharing-screen'));
        }
      }

    }, 1000);*/
  }

  /*  private restartRecording(): void {
      /!*    this.mediaRecorder.pause();
          setTimeout(() => {
            this.mediaRecorder.resume();
          }, 0);*!/

      this.stopRecording();
      console.log('restarting...');
      setTimeout(() => {
        this.startRecordingNew();
      }, 0);
    }*/


  private stopRecording(mode: RECORDING_MODE, canConcatenate = false, callback?): void {
    if (mode === RECORDING_MODE.CANVAS) {
      /*if (this.mediaRecorder && this.mediaRecorder.state === 'recording') {
        this.mediaRecorder.stop();
        this.recorder.stopRecording(this.stopAudioRecordingCallback.bind(this));
        setTimeout(() => {
          if (this.recordedBlobs && this.recordedBlobs.length > 0) {
            this.arrayOfRecordedBlobs.push(...this.recordedBlobs);
            /!* this.concatenateBlobs(this.arrayOfRecordedBlobs);
          oncatenateBlobs(this.arrayOfRecordedBlobs, 'video/webm;codecs=vp9,opus', (resultingBlob) => {
             this.recordedBlobsConcatenated = resultingBlob;
             console.log(this.recordedBlobsConcatenated);
           });*!/
          }
        }, 0);
      }*/
      if (this.mediaRecorder) {
        this.mediaRecorder.stopRecording(() => {
          const blob = this.mediaRecorder.getBlob();
          this.arrayOfRecordedBlobs.push(blob);
          this.currentRecordingText = 'Recording stopped';
          this.ref.detectChanges();
          if (callback) {
            callback(mode);
          }


          if (canConcatenate) {
            this.handleBlobsMerge().then();
          }

        });
      }

    } else if (mode === RECORDING_MODE.HTML_ELEMENT) {
      /* with CanvasRecorder only
      this.htmlCanvasRecorder.stop((blob) => {
        this.html2canvasIsRecording = false;
        this.arrayOfRecordedBlobs.push(blob);
        if (canConcatenate) {
          this.handleBlobsMerge().then();
        }
      });*/
      if (this.htmlCanvasRecorder) {
        this.htmlCanvasRecorder.stopRecording(() => {
          this.html2canvasIsRecording = false;
          const blob = this.htmlCanvasRecorder.getBlob();
          this.arrayOfRecordedBlobs.push(blob);
          this.currentRecordingText = 'Recording stopped';
          this.ref.detectChanges();
          if (callback) {
            callback(mode);
          }

          if (canConcatenate) {
            this.handleBlobsMerge().then();
          }
        });
      }

    }


  }

  private startHTMLRecording(): void {
    console.log('startHTMLRecording');
    this.elementToRecord = this.currentActiveCanvasData.el;
/*    this.htmlCanvasRecorder = new CanvasRecorder(this.elementToRecord, {
      disableLogs: false,
      useWhammyRecorder: true,
      mimeType: 'video/x-matroska;codecs=avc1',
    });
    this.htmlCanvasRecorder.record();*/
    /*this.htmlCanvasRecorder.stop((blob) => {
      video.src = URL.createObjectURL(blob);
    });*/
    this.canvas2d = document.getElementById('background-canvas');
    this.context = this.canvas2d.getContext('2d');
    this.canvas2d.width = this.elementToRecord.clientWidth;
    this.canvas2d.height = this.elementToRecord.clientHeight;
    // this.showPlayer(this.canvas2d.captureStream());
    this.htmlCanvasRecorder = new RecordRTC(this.canvas2d, {
      type: 'canvas',
      mimeType: 'video/x-matroska;codecs=avc1',
    });
    this.htmlLooper();
    this.htmlCanvasRecorder.startRecording();
    this.html2canvasIsRecording = true;
    this.currentRecordingText = 'HTML Recording started';
    this.ref.detectChanges();
    this.recordButtonTextContent = 'Stop Recording';
  }

  private startCanvasRecording($event?: MouseEvent): void {
    console.log('startCanvasRecording');
    /*    this.stream = this.currentActiveCanvasData.el.captureStream();

        let options = { mimeType: 'video/webm;codecs=vp9,opus' };
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
          console.error(`${options.mimeType} is not supported`);
          options = { mimeType: 'video/webm;codecs=vp8,opus' };
          if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            console.error(`${options.mimeType} is not supported`);
            options = { mimeType: 'video/webm' };
            if (!MediaRecorder.isTypeSupported(options.mimeType)) {
              console.error(`${options.mimeType} is not supported`);
              options = { mimeType: '' };
            }
          }
        }


        try {
          this.mediaRecorder = new MediaRecorder(this.stream, options);
          this.startRecordingAudio();
          this.recordButtonTextContent = 'Stop Recording';
          this.mediaRecorder.onstop = (event) => {
            // console.log('Recorder stopped: ', event);
            // console.log('Recorded Blobs: ', this.recordedBlobs);
          };
          this.mediaRecorder.ondataavailable = this.handleDataAvailable.bind(this);
          this.mediaRecorder.start();
          // console.log('MediaRecorder started', this.mediaRecorder);

          this.showPlayer();

        } catch (e) {
          console.error('Exception while creating MediaRecorder:', e);
          return;
        }*/


    this.mediaRecorder = new RecordRTC(this.currentActiveCanvasData.el, {
      type: 'canvas',
      mimeType: 'video/x-matroska;codecs=avc1',
    });

    this.mediaRecorder.startRecording();
    this.currentRecordingText = 'CANVAS Recording started';
    this.ref.detectChanges();
    this.recordButtonTextContent = 'Stop Recording';
  }

  private async concatenateBlobs(arrayOfRecordedBlobs: any[], blobType): Promise<void> {
    return new Promise((resolve, reject) => {
      try {
        ConcatenateBlobs(arrayOfRecordedBlobs, blobType, (resultingBlob) => {
          console.log('concatenateBlobs ok!');
          resolve(resultingBlob);
        });
      } catch (e) {
        reject(e);
      }
    });
    /*   const blobs = [];
       arrayOfRecordedBlobs.map((chunks: Blob[]) => {
         chunks.map(blob => {
           blobs.push(blob);
         });
       });

       const superBuffer = new Blob(blobs, { type: 'video/webm' });

       this.recordedBlobsConcatenated = [superBuffer];*/

  }

  html2canvasRecordingClick($event?: MouseEvent): void {
    if (!this.html2canvasIsRecording) {
      console.log('html2canvasRecordingClick');
      // this.elementToRecord = document.querySelector('.active-video-container__wrap');
      this.elementToRecord = this.currentActiveCanvasData.el;
      this.canvas2d = document.getElementById('background-canvas');
      this.context = this.canvas2d.getContext('2d');

      this.canvas2d.width = this.elementToRecord.clientWidth;
      this.canvas2d.height = this.elementToRecord.clientHeight;

      this.htmlCanvasRecorder = new RecordRTC(this.canvas2d, {
        type: 'canvas',
        mimeType: 'video/webm',
      });

      this.html2canvasIsRecording = true;
      this.htmlLooper();
      this.htmlCanvasRecorder.startRecording();
    } else {
      this.htmlCanvasRecorder.stopRecording(() => {
        this.html2canvasIsRecording = false;

        const blob = this.htmlCanvasRecorder.getBlob();
        // document.getElementById('preview-video').srcObject = null;
        $('#ZMR_PLAYER').css('display', 'block');
        const zmrPlayer: any = document.querySelector('#ZMR_PLAYER video');
        const url = URL.createObjectURL(blob);
        zmrPlayer.src = url;

        const a: any = document.createElement('a');
        document.body.appendChild(a);
        a.style = 'display: none';
        a.href = url;
        a.download = 'htmlRecord.webm';
        a.click();
        window.URL.revokeObjectURL(url);

        // document.getElementById('preview-video').parentNode.style.display = 'block';


        // window.open(URL.createObjectURL(blob));
      });
    }
  }

  private startRecordingAudio(): void {
    if (!this.microphone) {
      this.captureMicrophone((mic) => {
        this.microphone = mic;
        const options = {
          type: 'audio',
          numberOfAudioChannels: this.isEdge ? 1 : 2,
          checkForInactiveTracks: true,
          bufferSize: 16384,
          recorderType: null,
          sampleRate: null,
        };

        if (this.isSafari || this.isEdge) {
          options.recorderType = StereoAudioRecorder;
        }

        if (navigator.platform && navigator.platform.toString().toLowerCase().indexOf('win') === -1) {
          options.sampleRate = 48000; // or 44100 or remove this line for default
        }

        this.recorder = RecordRTC(this.microphone, options);

        this.recorder.startRecording();
      });
    }
  }

  private captureMicrophone(callback: (mic) => void): void {
    if (this.microphone) {
      callback(this.microphone);
      return;
    }

    if (typeof navigator.mediaDevices === 'undefined' || !navigator.mediaDevices.getUserMedia) {
      alert('This browser does not supports WebRTC getUserMedia API.');

      if (!!navigator.getUserMedia) {
        alert('This browser seems supporting deprecated getUserMedia API.');
      }
    }

    navigator.mediaDevices.getUserMedia({
      audio: this.isEdge ? true : {
        echoCancellation: false
      }
    }).then((mic) => {
      callback(mic);
    }).catch((error) => {
      alert('Unable to capture your microphone. Please check console logs.');
      console.error(error);
    });
  }

  private stopAudioRecordingCallback(): void {
    const blob = this.recorder.getBlob();
    this.audioRecorded.src = URL.createObjectURL(blob);
    this.audioRecorded.play();
    setTimeout(() => {
      this.downloadAudioRecording(blob);
    }, 200);
  }

  private htmlLooper(): void {
    const elementToRecord = this.elementToRecord;
    this.htmlLooperSI = setInterval(() => {
      /* html2canvas(elementToRecord).then((canvas) => {
         this.context.clearRect(0, 0, this.canvas2d.width, this.canvas2d.height);
         this.context.drawImage(canvas, 0, 0, this.canvas2d.width, this.canvas2d.height);

         if (!this.html2canvasIsRecording) {
           return;
         }

         requestAnimationFrame(this.htmlLooper.bind(this));
       });*/

      html2canvas(this.elementToRecord, {
        onrendered: (canvas) => {
          this.context.clearRect(0, 0, this.canvas2d.width, this.canvas2d.height);
          this.context.drawImage(canvas, 0, 0, this.canvas2d.width, this.canvas2d.height);

          if (!this.html2canvasIsRecording) {
            return;
          }

          requestAnimationFrame(this.htmlLooper.bind(this));
        }
      });

      clearInterval(this.htmlLooperSI);
      this.htmlLooperSI = null;
    }, 200);
  }

  restartVideoRecording(recordingMode): void {
    console.log('restarting Video Recording...');
    switch (recordingMode) {
      case RECORDING_MODE.CANVAS :
        this.startCanvasRecording();
        break;
      case RECORDING_MODE.HTML_ELEMENT :
        this.startHTMLRecording();
        break;
    }
  }

  stopRecordingCallback(recorder): void{
    const blob = this.mediaRecorder.getBlob();
    this.arrayOfRecordedBlobs.push(blob);
    this.currentRecordingText = 'Recording stopped';
    this.ref.detectChanges();
    console.log('rererer');
    recorder = null;
    if (this.isRecording) {
      this.restartVideoRecording(this.currentActiveCanvasData.recordingMode);
    }
  }

  private recordElementChanged(): void {
    if (this.isRecording) {
      if (this.mediaRecorder) {
        this.mediaRecorder.stopRecording(this.stopRecordingCallback.bind(this)(this.mediaRecorder));
      } else if (this.htmlCanvasRecorder) {
        this.html2canvasIsRecording = false;
        this.htmlCanvasRecorder.stopRecording(this.stopRecordingCallback.bind(this)(this.htmlCanvasRecorder));
      } else {
        this.restartVideoRecording(this.currentActiveCanvasData.recordingMode);
      }
    }


   /* if (this.recordButtonTextContent !== 'Start Recording') {
      this.stopRecording(this.prevActiveCanvasData.recordingMode, false, this.restartVideoRecording.bind(this));
      console.log('restarting record...');
    } else {
      this.restartVideoRecording(this.prevActiveCanvasData.recordingMode);
    }*/
  }

  ZMR_PLAYER_Record($event: MouseEvent): void {
    const options = {mimeType: 'video/webm'};
    const zmrPlayer: any = document.querySelector('#ZMR_PLAYER video');
    const mirror: any = document.querySelector('#mirror');
    const recordedBlobs = [];
    const stream = zmrPlayer.captureStream();
    mirror.srcObject = stream;
    try {
      const mediaRecorder = new MediaRecorder(stream, options);
      mediaRecorder.onstop = (event) => {
        console.log('Recorder stopped: ', event);
        const superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
        zmrPlayer.srcObject = window.URL.createObjectURL(superBuffer);
      };

      mediaRecorder.ondataavailable = (event) => {
        console.log('Recorder stopped: ', event);
        recordedBlobs.push(event.data);
      };
    } catch (e) {
      console.log(e);
    }
  }

  private async handleBlobsMerge(): Promise<void> {
    console.log('mediaRecorder stopped');
    this.recorder.stopRecording(this.stopAudioRecordingCallback.bind(this));

    console.log('concatenating blobs...');
   /* this.recordedBlobsConcatenated = await this.concatenateBlobs(
      this.arrayOfRecordedBlobs,
      this.arrayOfRecordedBlobs[0].type
    );*/

    this.recordedBlobsConcatenated =
      this.arrayOfRecordedBlobs.reduce(
        (accumulator, currentValue) => new Blob([accumulator, currentValue], {type: 'video/webm; codecs=vorbis,vp8'})
      );
    // this.recordedBlobsConcatenated = new Blob(this.arrayOfRecordedBlobs, {type: 'video/webm; codecs=vorbis,vp8'});

    console.log('concatenating blobs done!');
  }
}
