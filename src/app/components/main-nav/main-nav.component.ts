import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {IActiveCanvasData, TResult, ZOOM_ACTIONS, ZoomService} from '../../views/web-rtc/zoom/services/zoom.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.less']
})
export class MainNavComponent {

  public isZoomMeetingShown = false;
  public isRecording = false;
  public zoomContainer = 'zmmtg-root';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  public showZoomSubmenu = true;

  constructor(
    private breakpointObserver: BreakpointObserver,
    public zoomService: ZoomService,
  ) {}

  toggleZoomWindow($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.isZoomMeetingShown = !this.isZoomMeetingShown;
    document.getElementById(this.zoomContainer).style.zIndex =
      this.isZoomMeetingShown ? '0' : '-1';
  }

  playRecording($event: MouseEvent): void {

  }

  handleRecordingBtnClick($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.zoomService.actionDataSubject.next(
      new TResult<{ triggerAction: string, params: object }>(
        true,
        ZOOM_ACTIONS.GENERIC,
        {
          triggerAction: 'HANDLE_RECORDING',
          params: {
            eventData: $event
          }
        }
      )
    );
  }

  downloadVideoRecording($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.zoomService.actionDataSubject.next(
      new TResult<{ triggerAction: string, params: object }>(
        true,
        ZOOM_ACTIONS.GENERIC,
        {
          triggerAction: 'DOWNLOAD_VIDEO',
          params: {
            eventData: $event
          }
        }
      )
    );
  }

  downloadAudioRecording($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.zoomService.actionDataSubject.next(
      new TResult<{ triggerAction: string, params: object }>(
        true,
        ZOOM_ACTIONS.GENERIC,
        {
          triggerAction: 'DOWNLOAD_AUDIO',
          params: {
            eventData: $event
          }
        }
      )
    );
  }
}
