// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  meetConfig: {
    apiKey: 'Aa1I2GFDRceWeQcuCpRWZw',
    meetingNumber: '3044848838',
    leaveUrl: '/web-rtc/zoom/zoom-demo',
    username: 'Tessa',
    signatureEndpoint: 'https://zoomjwtapp.herokuapp.com/',
    userEmail: 'tessa@yoursite.com',
    meetingPassword: 'WTJoLzBqUnQ2SFZTQXdJK2NaMjZCQT09', // if required
    role: 0, // 1 for host; 0 for attendee,
    meetingLink: 'https://zoom.us/j/3044848838?pwd=WTJoLzBqUnQ2SFZTQXdJK2NaMjZCQT09'
  }

/*  meetConfig: {
    apiKey: 'Aa1I2GFDRceWeQcuCpRWZw',
    meetingNumber: '5594680910',
    leaveUrl: '/web-rtc/zoom/zoom-demo',
    username: 'Tessa',
    signatureEndpoint: 'https://zoomjwtapp.herokuapp.com/',
    userEmail: 'tessa@yoursite.com',
    meetingPassWord: 'QTV4dVlheEpmMkpXRTgxMlFJeXNYZz09', // if required
    role: 0, // 1 for host; 0 for attendee
    meetingLink: 'https://zoom.us/j/5594680910?pwd=QTV4dVlheEpmMkpXRTgxMlFJeXNYZz09'
  }*/
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
